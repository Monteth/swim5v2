package com.ct8.monteth.swim5c

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import kotlinx.android.synthetic.main.fragment_compass.*


class CompassFr : Fragment(), SensorEventListener {
    private lateinit var sensorManager: SensorManager
    private var currentDegree = 0f

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    override fun onSensorChanged(event: SensorEvent?) {
        val degree = Math.round(event?.values?.get(0)!!)
        frCom_tvData.text = "Heading: " + java.lang.Float.toString(degree.toFloat()) + " degrees"
        // create a rotation animation (reverse turn degree degrees)
        val ra = RotateAnimation(currentDegree, (-1 * degree).toFloat(), Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        // how long the animation will take place
        ra.duration = 210
        // set the animation after the end of the reservation status
        ra.fillAfter = true
        // Start the animation
        frCom_ivCompass.startAnimation(ra)

        currentDegree = (-degree).toFloat()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        sensorManager = activity?.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        return inflater.inflate(R.layout.fragment_compass, container, false)
    }

    override fun onResume() {
        super.onResume()
        var sensor: Sensor? //sensorManager.getSensorList(Sensor.TYPE_ORIENTATION)?.get(0)
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION)
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
    }
}