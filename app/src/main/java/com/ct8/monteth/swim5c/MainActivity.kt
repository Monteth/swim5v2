package com.ct8.monteth.swim5c

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_compass -> {
                topnav_tvTitle.text = "Compass"
                acMain_pager.currentItem = 0
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_accelerometer -> {
                topnav_tvTitle.text = "Accelerometer"
                acMain_pager.currentItem = 1
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val fragments = mutableListOf<Fragment>()
        fragments.add(Fragment.instantiate(this, CompassFr::class.java.name))
        fragments.add(Fragment.instantiate(this, AccelerometerFr::class.java.name))
        val titles = arrayOf("Light", "Accelerometer", "Locations")
        acMain_pager.adapter = CustomPagerAdapter(supportFragmentManager, fragments.toTypedArray(), titles)

        acMain_bottomNav.setOnClickListener{}
        acMain_bottomNav.setOnTouchListener { _, _ ->  true}
        acMain_bottomNav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    class CustomPagerAdapter(fm: android.support.v4.app.FragmentManager, private val fragments: Array<android.support.v4.app.Fragment>, private val titles: Array<String>) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int) = fragments[position]
        override fun getCount() =  fragments.size
        override fun getPageTitle(position: Int) = titles[position]
    }
}
