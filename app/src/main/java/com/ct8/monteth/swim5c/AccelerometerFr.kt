package com.ct8.monteth.swim5c
import android.content.Context
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ct8.monteth.swim5c.R.id.frCom_ivCompass
import kotlinx.android.synthetic.main.fragment_accelerometer.*
import kotlinx.android.synthetic.main.fragment_compass.*

class AccelerometerFr : Fragment(), SensorEventListener {
    private lateinit var sensorManager: SensorManager

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    override fun onSensorChanged(event: SensorEvent?) {
        val stringBuilder = StringBuilder()

        stringBuilder.append("X acceleration: ")
        stringBuilder.append(String.format("%7.4f", event?.values?.get(0)))
        stringBuilder.append(" m/s\u00B2\nY acceleration: ")
        stringBuilder.append(String.format("%7.4f", event?.values?.get(1)))
        stringBuilder.append(" m/s\u00B2\nZ acceleration: ")
        stringBuilder.append(String.format("%7.4f", event?.values?.get(2)))
        stringBuilder.append(" m/s\u00B2")

        if (event?.values?.get(1)!! > 5) {
            frAcc_ivColor.setBackgroundColor(Color.RED)
        }else{
            frAcc_ivColor.setBackgroundColor(Color.GREEN)
        }

        frAcc_tvData.text = stringBuilder
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        sensorManager = activity?.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        return inflater.inflate(R.layout.fragment_accelerometer, container, false)
    }

    override fun onResume() {
        super.onResume()
        val sensor: Sensor? = sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER)?.get(0)
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
    }
}